import React,{useContext} from 'react';
import countryStyle from './AllCountries.module.css';
import ThemeContext from '../theme/Theme';

function AllCountries({ countries, searchQuery}) {
    const { darkMode, setDarkMode } = useContext(ThemeContext);

    const filteredCountries = countries.filter((country) => {
        return country.name.common.toLowerCase().includes(searchQuery.toLowerCase());
    });

   
    return (
        <>
            {filteredCountries.length === 0 ?
                <p className={countryStyle.notFound}
                    style={{ color: darkMode ? 'white' : 'black' }}
                >
                    Not found
                </p> :
                <ul className={countryStyle.countriesContainer}
                    style={{ backgroundColor: darkMode ? '#202D36' : '#FAFAFA', color: darkMode ? 'white' : 'black' }}
                >
                    {filteredCountries.map((country, index) => (
                        <li key={index} className={countryStyle.eachCountry}
                            style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}
                        >
                            <div className={countryStyle.countryImgContainer}>
                                <img
                                    src={country.flags.png}
                                    alt={country.flags.alt}
                                    className={countryStyle.countryImg}
                                />
                            </div>
                            <div className={countryStyle.countryDetails}>
                                <div className={countryStyle.countryName}>
                                    <p>{country.name.common.slice(0, 35)}</p>
                                </div>

                                <div className={countryStyle.countryInfo}>
                                    <p>Population: {country.population}</p>
                                    <p>Region: {country.region}</p>
                                    <p>Capital: {country.capital} </p>
                                    <p>Sub-region: {country.subregion}</p>
                                    <p>Area: {country.area}</p>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
            }
        </>
    );
}

export default AllCountries;
