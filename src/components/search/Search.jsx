
import React,{useContext} from 'react';
import searchStyle from './Search.module.css';
import { AiOutlineSearch } from 'react-icons/ai';
import ThemeContext from '../theme/Theme';

function Search({ setSearchQuery}) {
  const { darkMode, setDarkMode } = useContext(ThemeContext);
  
  const handleInputChange = (e) => {
    setSearchQuery(e.target.value);
  };

  return (
    <>
      <div className={searchStyle.searchContainer}
        style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}
      >
        <div className={searchStyle.searchIconContainer}>
          <span className={searchStyle.searchIcon}> <AiOutlineSearch />  </span>
        </div>
        <div className={searchStyle.searchInput} >
          <input
            type='text'
            placeholder='Search for a country...'
            size='35'
            className={searchStyle.inputText}
            onChange={handleInputChange}
            style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}
          />
        </div>
      </div>
    </>
  );
}

export default Search;
