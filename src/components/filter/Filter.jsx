
import React from 'react';
import Custom from '../customSelect/CustomSelect';

const Filter = ({ updateCountriesByRegion, darkMode }) => {

    const options = [
        { sortType: 'Africa', label: 'Sort by region' },
        { sortType: 'Americas', label: 'Sort by region' },
        { sortType: 'Asia', label: 'Sort by region' },
        { sortType: 'Europe', label: 'Sort by region' },
        { sortType: 'Oceania', label: 'Sort by region' },
        { sortType: 'All', label: 'Sort by region' },   
      ];

      function handleSelect(option) {
          updateCountriesByRegion(option.sortType)
      }

    
    return (
        <div className="app">
             <Custom options={options} handleSelect={handleSelect} darkMode={darkMode} filterBy='region' />
        </div>
    );
};

export default Filter;



