
import React from 'react';
import Custom from '../customSelect/CustomSelect';


const SortPopulation = ({ sortPopulation }) => {

  const options = [
    { sortType: 'ascending', label: 'Sort by population' },
    { sortType: 'descending', label: 'Sort by population' },
    { sortType: 'All', label: 'Sort by population' },
  ];

  function handleSelect(option) {
    sortPopulation(option.sortType)
    console.log(option.sortType,"inside-population")
  }
  return (
    <div className="app">

      <Custom options={options} handleSelect={handleSelect}  filterBy='population' />

    </div>
  );
};

export default SortPopulation;




