import React from 'react';
import Custom from '../customSelect/CustomSelect';


const SortArea = ({ sortArea }) => {
   
    const options = [
        { sortType: 'ascending', label: 'Sort by Area' },
        { sortType: 'descending', label: 'Sort by Area' },
        { sortType: 'All', label: 'Sort by Area' },
      ];

      function handleSelect(option) {
           sortArea(option.sortType)
           console.log(option.sortType,"inside-area")
      }

    return (
        <div className="app">
             <Custom options={options} handleSelect={handleSelect}  filterBy='area' />
        </div>
    );
};

export default SortArea;



