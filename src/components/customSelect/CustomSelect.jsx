import React, { useState,useContext } from 'react';
import styles from './CustomSelect.module.css';
import { MdExpandLess, MdExpandMore } from 'react-icons/md';
import ThemeContext from '../theme/Theme';

const Custom = ({ options, handleSelect,filterBy }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  const { darkMode, setDarkMode } = useContext(ThemeContext);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option);
    setIsOpen(false);
    handleSelect(option)
  };

  return (
    <div className={styles.filterContainer}>
      <div
        className={`${styles.dropdownToggle} ${isOpen ? styles.open : ''}`}
        onClick={toggleDropdown}
        style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}
      >
        {selectedOption ? filterBy === 'subregion' ? selectedOption : selectedOption.sortType : `Sort by ${filterBy}`}
        <span className={styles.icon}>
          {isOpen ? <MdExpandMore /> : <MdExpandLess />}
        </span>
      </div>
      <ul className={`${styles.dropdownMenu} ${isOpen ? styles.open : ''}`}
        style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}
      >
        {options && options.map((option, index) => (
          <li key={index} onClick={() => handleOptionClick(option)}
            style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}
          >
            {filterBy === 'subregion' ? option : option.sortType}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Custom;



