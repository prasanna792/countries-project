import React, { useEffect, useState,useContext } from 'react';
import appStyles from './App.module.css';
import axios from 'axios';
import Header from './components/header/Header';
import Search from './components/search/Search';
import Filter from './components/filter/Filter';
import AllCountries from './components/allCountries/AllCountries.jsx';
import FilterSubregion from './components/filterSubregion/FilterSubregion';
import SortPopulation from './components/sortByPopulation/SortPopulation';
import SortArea from './components/sortByArea/SortArea';
import ThemeContext from './components/theme/Theme';

function App() {
  const [searchQuery, setSearchQuery] = useState('');
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [selectedRegion, setSelectedRegion] = useState(null);
  const [selectedSubregion, setSelectedSubregion] = useState(null);
  const [population, setPopulation] = useState(null);
  const [area, setArea] = useState(null);
  const { darkMode, setDarkMode } = useContext(ThemeContext);

  useEffect(() => {
    axios
      .get('https://restcountries.com/v3.1/all')
      .then((response) => {
        if (response.status !== 200) {
          console.log('Fetching data failed');
          setError(true);
          setLoading(false);
          throw new Error('Fetching data failed');
        } else {
          return response.data;
        }
      })
      .then((data) => {
        if (data.length === 0) {
          setLoading(false);
          setError(false);
        } else {
          setCountries(data);
          setLoading(false);
          setError(false);
        }
      })
      .catch((err) => {
        setError(true);
        setLoading(false);
        console.error(err, 'Fetching error');
      });
  }, []);


  const updateCountriesByRegion = (region) => {
    setSelectedRegion(region);
    if (selectedRegion === 'All') {
      setSelectedSubregion(null);
    }
  };

  const updateCountriesBySubregion = (subregion) => {
    setSelectedSubregion(subregion);
  };


  const allSubRegions = countries.reduce((acc, current) => {
    if (!acc[current.region]) {
      acc[current.region] = [];
    }
    if (current.subregion && !acc[current.region].includes(current.subregion)) {
      acc[current.region].push(current.subregion);
    }

    return acc;
  }, {});



  let filteredCountries;

  // filtering by region and subregion
  filteredCountries = countries.filter((country) => {
    if (selectedRegion === 'All') {
      return true;
    }
    if (selectedRegion && selectedSubregion) {
      return country.region === selectedRegion && country.subregion === selectedSubregion;

    } else if (selectedRegion) {

      return country.region === selectedRegion;
    } else if (selectedSubregion) {
      return country.subregion === selectedSubregion;
    }
    return true;
  });


  // sorting by population and area

  filteredCountries = filteredCountries.sort((first, second) => {

    if(population === 'All' || area === 'All') {
      return 0;
    }
    if (population === 'ascending') {
      return first.population - second.population;
    } else if (population === 'descending') {
      return second.population - first.population;
    } else if (area === 'ascending') {
      return first.area - second.area;
    } else if (area === 'descending') {
      return second.area - first.area;
    }
  });



  function sortPopulation(type) {
    setPopulation(type);
    setArea(null);

  }

  function sortArea(type) {
    setArea(type);
    setPopulation(null);
    console.log(area, "area");
  }

  return (
    <div>
      <Header />
      <div className={appStyles.searchFilter}
        style={{ backgroundColor: darkMode ? '#202D36' : '#FAFAFA', color: darkMode ? 'white' : 'black' }}
      >
        <Search setSearchQuery={setSearchQuery}  />
        <SortArea sortArea={sortArea} />
        <SortPopulation  sortPopulation={sortPopulation} />
        {selectedRegion !== 'All' && <FilterSubregion
          allSubRegions={selectedRegion && allSubRegions[selectedRegion]}
          updateCountriesBySubregion={updateCountriesBySubregion}
          
        />}

        <Filter updateCountriesByRegion={updateCountriesByRegion}  />
      </div>
      <section>
        {loading && <div className={appStyles.ldsDualRing}></div>}
        {error && (
          <div className={appStyles.errorMessage}>
            <h2>Something went wrong</h2>
            <h5>Fetching data failed.</h5>
          </div>
        )}

        {!loading && (selectedRegion || selectedSubregion || population || area) ? (
          <AllCountries
            countries={filteredCountries}
            searchQuery={searchQuery}
            darkMode={darkMode}
          />
        ) : !loading && !error && !selectedRegion && (
          <AllCountries
            countries={countries}
            searchQuery={searchQuery}
            darkMode={darkMode}
          />
        )}
      </section>
    </div>
  );
}

export default App;
